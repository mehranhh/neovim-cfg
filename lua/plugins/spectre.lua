return {
    "windwp/nvim-spectre",
    opts = {
        mapping = {
            ['toggle_line'] = {
                map = "o",
                cmd = "<cmd>lua require('spectre').toggle_line()<CR>",
                desc = "toggle current item"
            },
            ['enter_file'] = {
                map = "<cr>",
                cmd = "<cmd>lua require('spectre.actions').select_entry()<CR>",
                desc = "goto current file"
            },
            ['send_to_qf'] = {
                map = "<leader>Q",
                cmd = "<cmd>lua require('spectre.actions').send_to_qf()<CR>",
                desc = "send all item to quickfix"
            },
            ['replace_cmd'] = {
                map = "<leader>C",
                cmd = "<cmd>lua require('spectre.actions').replace_cmd()<CR>",
                desc = "input replace vim command"
            },
            ['show_option_menu'] = {
                map = "<leader>o",
                cmd = "<cmd>lua require('spectre').show_options()<CR>",
                desc = "show option"
            },
            ['run_replace'] = {
                map = "<leader>R",
                cmd = "<cmd>lua require('spectre.actions').run_replace()<CR>",
                desc = "replace all"
            },
            ['change_view_mode'] = {
                map = "<leader>v",
                cmd = "<cmd>lua require('spectre').change_view()<CR>",
                desc = "change result view mode"
            },
            ['toggle_live_update']={
                map = "tu",
                cmd = "<cmd>lua require('spectre').toggle_live_update()<CR>",
                desc = "update change when vim write file."
            },
            ['toggle_ignore_case'] = {
                map = "ti",
                cmd = "<cmd>lua require('spectre').change_options('ignore-case')<CR>",
                desc = "toggle ignore case"
            },
            ['toggle_ignore_hidden'] = {
                map = "th",
                cmd = "<cmd>lua require('spectre').change_options('hidden')<CR>",
                desc = "toggle search hidden"
            },
            -- you can put your mapping here it only use normal mode
        },
    },
    config = function(_, opts)
        local spectre = require('spectre');
        spectre.setup(opts);

        local status_ok, wk = pcall(require, "which-key")
        if not status_ok then
            return
        end

        wk.add({
            { "<leader>rR", function () spectre.open_file_search() end, desc = "Replace Current File" },
            { "<leader>rs", function () spectre.open() end, desc = "Replace Workspace" },
            { "<leader>rS", function () spectre.open_visual() end, desc = "Replace Workspace (Visual)" },
            { "<leader>rS", function () spectre.open_visual({ select_word = true }) end, desc = "Replace Workspace (Word)" },
        });
    end,
}
